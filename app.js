const express = require('express')
const app = express()
const morgan = require("morgan")
const bodyParser = require("body-parser")
const port = 3000
const postRoutes = require('./routes/post')

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use("/", postRoutes);


app.listen(port, () => {
    console.log(`app is running in port: ${port} `)
});

