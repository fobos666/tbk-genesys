var request = require('request');

exports.getPost = (req, res) => {
    res.json({
        post: {status:"ok"}        
    });
};

exports.sendPost = (req, res) => {
    var user = req.body.user
    var securek = req.body.sec
    var alias = req.body.alias
    var chatid = req.body.chat
    var msg = req.body.msg 
    var url = 'https://chat.comercios.qa.transbank.cl/genesys/2/chat/Desarrollo/'+ chatid +'/send'

    var options = {
        'method': 'POST',
        'url': url,
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-API-Key': 'YXBwdGJrLmFzaXN0ZW50ZXZpcnR1YWw6YjM2MDNhZWQ2YjgyYTE2ZmYwOTU3N2UyZWM0OWYzYWY3ZGRmMTAyM2Q3ZjkwZDM4NTE2OWIzYTZmODI4ZmY0MA==',
            'Host': 'chat.comercios.qa.transbank.cl'
        },
        form: {
            'message': msg,
            'userId': user,
            'secureKey': securek,
            'alias': alias,
            'messageType': 'text'
        }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            respuesta = response.body;
            res.status(200).json(response.body);
        });

};

exports.refreshPost = (req, res) => {
    var user = req.body.user
    var securek = req.body.sec
    var alias = req.body.alias
    var chatid = req.body.chat
    var url = 'https://chat.comercios.qa.transbank.cl/genesys/2/chat/Desarrollo/'+ chatid +'/refresh'
    
    var request = require('request');
    var options = {
        'method': 'POST',
        'url': url,
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-API-Key': 'YXBwdGJrLmFzaXN0ZW50ZXZpcnR1YWw6YjM2MDNhZWQ2YjgyYTE2ZmYwOTU3N2UyZWM0OWYzYWY3ZGRmMTAyM2Q3ZjkwZDM4NTE2OWIzYTZmODI4ZmY0MA==',
            'Host': 'chat.comercios.qa.transbank.cl'
        },
        form: {
            'userId': user,
            'secureKey': securek,
            'alias': alias
         }
    };
    
    request(options, function (error, response) {
        if (error) throw new Error(error);
        respuesta = response.body;
        res.status(200).json(response.body);
    });
    
};

exports.endPost = (req, res) => {
    console.log(req.body);
    res.json({
        post: {ennviado:"ok"}        
    });
};

exports.createPost = (req, res) => {
    var nombre = req.body.nombre
    var apellido = req.body.apellido
    var nick = req.body.nombre
    var mensaje = req.body.mensaje
    var respuesta = ""

    console.log("request body", req.body)
    var url = 'https://chat.comercios.qa.transbank.cl/genesys/2/chat/Desarrollo?firstName='+ nombre +'&lastName='+ apellido +'&nickname='+ nick +'&subject='+ mensaje

    var options = {
        'method': 'POST',
        'url': url,
        'headers': {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-API-Key': 'YXBwdGJrLmFzaXN0ZW50ZXZpcnR1YWw6YjM2MDNhZWQ2YjgyYTE2ZmYwOTU3N2UyZWM0OWYzYWY3ZGRmMTAyM2Q3ZjkwZDM4NTE2OWIzYTZmODI4ZmY0MA==',
          'Host': 'chat.comercios.qa.transbank.cl'
        }
    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        respuesta = response.body;
        res.status(200).json(response.body);
    });
};