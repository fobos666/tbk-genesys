const { Router } = require('express');
const express = require('express')
const postController = require('../controllers/post')

const router = express.Router();

router.get("/", postController.getPost)
router.post("/send", postController.sendPost)
router.post("/create", postController.createPost)
router.post("/refresh", postController.refreshPost)
router.post("/end", postController.endPost)


module.exports = router;

